# Kubectl Helm

Docker image with kubectl and helm preinstalled. Supports both amd64 and arm64

Image always has latest kubectl from build time and specified version of helm

Alpine Based:

`registry.gitlab.com/cmmarslender/kubectl-helm:$HELM_VERSION`

Debian Based: 

`registry.gitlab.com/cmmarslender/kubectl-helm:$HELM_VERSION-debian`

Available tags can be [refereced here](https://gitlab.com/cmmarslender/kubectl-helm/container_registry)
